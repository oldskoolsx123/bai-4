package org.example;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter name of student A: ");
        String nameA = scanner.nextLine();
        System.out.print("Enter score of student A: ");
        int scoreA = scanner.nextInt();
        scanner.nextLine();

        System.out.print("Enter name of student B: ");
        String nameB = scanner.nextLine();
        System.out.print("Enter score of student B: ");
        int scoreB = scanner.nextInt();

        Student studentA = new Student(nameA, scoreA);
        Student studentB = new Student(nameB, scoreB);
        if(studentA.getDiem() < studentB.getDiem()){
            System.out.println(studentB.getName());
        }else{
            System.out.println(studentA.getName());
        }
    }
}